/**
 * Created by Bryan on 01/02/16.
 */
$(function () {

    console.log("ready");
    $('.carousel').carousel();

    $('.input-group.date').datepicker({
        format: "dd/mm/yy",
        language: "es",
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true
    });

    /*$("#InputTel").mask("99");*/
});